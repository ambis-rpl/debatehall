from django.shortcuts import render, redirect
from mengeloladebat.models import Debat
from main.models import Juri, HasilDebat

# def formulir_hasil(request):
#     return render(request, 'mengumumkanhasil/formulir_hasil.html')

def hasil_debat(request):
    return render(request, 'mengumumkanhasil/formulir_hasil.html')
  
def daftar_hasil(request):
    response = {}
    user = request.user
    response['isJuri'] = False
    if (user.is_authenticated):
      juri = Juri.objects.filter(user=user)
      if (len(juri) > 0):
        response['isJuri'] = True

    response['hasils'] = HasilDebat.objects.all()
    return render(request, 'mengumumkanhasil/daftar_hasil.html', response)
  
def getAllHasil(request):
    response = {}
    user = request.user
    juri = Juri.objects.filter(user=user)
    if (len(juri) < 1):
      return redirect('mengumumkanhasil:daftar_hasil')
    
    juri = juri[0]
    response['hasils'] = HasilDebat.objects.all()
    return render(request, 'mengumumkanhasil/dasbor_hasil.html', response)
  
def detail_hasil(request, id_hasil):
    response = {}
    response['hasil'] = HasilDebat.objects.filter(id_hasil=id_hasil)[0]
    return render(request, 'mengumumkanhasil/hasil_debat.html', response)

def showCreateArtikelForm(request):
    response = {}
    
    user = request.user
    juri = Juri.objects.filter(user=user)
    if (len(juri) < 1):
      return redirect('mengumumkanhasil:daftar_hasil')
    
    juri = juri[0]
    if request.method == 'POST':
      id_hasil = pm = request.POST.get('iddebat')
      iddebat = request.POST.get('iddebat')
      debat = Debat.objects.get(iddebat=request.POST.get('iddebat'))
      pm = request.POST.get('pm')
      dpm = request.POST.get('dpm')
      gw = request.POST.get('gw')
      pm_score  = request.POST.get('pm_score')
      dpm_score = request.POST.get('dpm_score')
      gw_score = request.POST.get('gw_score')
      oo = request.POST.get('oo')
      dlo = request.POST.get('dlo')
      ow = request.POST.get('ow')
      oo_score = request.POST.get('oo_score')
      dlo_score = request.POST.get('dlo_score')
      ow_score = request.POST.get('ow_score')
      general_comment = request.POST.get('general_comment')
      
      hasildebat = HasilDebat( id_hasil = id_hasil, debatModel= debat, \
                              pm=pm, dpm=dpm, gw=gw, \
                              pm_score=pm_score, dpm_score=dpm_score, gw_score=gw_score, \
                              oo=oo, dlo=dlo, ow=ow, \
                              oo_score=oo_score, dlo_score=dlo_score, ow_score=ow_score, \
                                  general_comment=general_comment, juri_pembuat=juri)
      if isValid(hasildebat):
        juri.buatHasilDebat(hasildebat)
        return redirect('mengumumkanhasil:dasbor_hasil')
      else:
        response['err_msg'] = True
        response['id_hasil'] = id_hasil
        response['iddebat'] = iddebat
        response['pm'] = pm
        response['dpm'] = dpm
        response['gw'] = gw
        response['pm_score'] = pm_score
        response['dpm_score'] = dpm_score
        response['gw_score'] = gw_score
        response['oo'] = oo
        response['dlo'] = dlo
        response['ow'] = ow
        response['oo_score'] = oo_score
        response['dlo_score'] = dlo_score
        response['ow_score'] = ow_score
        response['general_comment'] = general_comment        
        return render(request, 'mengumumkanhasil/formulir_hasil.html', response)

    else:
        return render(request, 'mengumumkanhasil/formulir_hasil.html', response)

def showFormWithDetailArtikel(request, id_hasil):
    response = {}
    user = request.user
    juri = Juri.objects.filter(user=user)
    if (len(juri) < 1):
      return redirect('mengumumkanhasil:daftar_hasil')
    
    juri = juri[0]
    response['id_hasil'] = id_hasil
    if request.method == 'POST':
      pm = request.POST.get('pm')
      dpm = request.POST.get('dpm')
      gw = request.POST.get('gw')
      pm_score  = request.POST.get('pm_score')
      dpm_score = request.POST.get('dpm_score')
      gw_score = request.POST.get('gw_score')
      oo = request.POST.get('oo')
      dlo = request.POST.get('dlo')
      ow = request.POST.get('ow')
      oo_score = request.POST.get('oo_score')
      dlo_score = request.POST.get('dlo_score')
      ow_score = request.POST.get('ow_score')
      general_comment = request.POST.get('general_comment')
      hasildebat = HasilDebat(pm=pm, dpm=dpm, gw=gw, \
                              pm_score=pm_score, dpm_score=dpm_score, gw_score=gw_score, \
                              oo=oo, dlo=dlo, ow=ow, \
                              oo_score=oo_score, dlo_score=dlo_score, ow_score=ow_score, \
                                  general_comment=general_comment)
      if isValid(hasildebat):
        juri.ubahHasilDebat(id_hasil, hasildebat)
        return redirect('mengumumkanhasil:dasbor_hasil')
      else:
        response['err_msg'] = True
        response['pm'] = pm
        response['dpm'] = dpm
        response['gw'] = gw
        response['pm_score'] = pm_score
        response['dpm_score'] = dpm_score
        response['gw_score'] = gw_score
        response['oo'] = oo
        response['dlo'] = dlo
        response['ow'] = ow
        response['oo_score'] = oo_score
        response['dlo_score'] = dlo_score
        response['ow_score'] = ow_score
        response['general_comment'] = general_comment  
        return render(request, 'mengumumkanhasil/ubah_hasil.html', response)        

    else:
      hasildebat = HasilDebat.objects.get(id_hasil=id_hasil)
      response['pm'] = hasildebat.pm
      response['dpm'] = hasildebat.dpm
      response['gw'] = hasildebat.gw
      response['pm_score'] = hasildebat.pm_score
      response['dpm_score'] = hasildebat.dpm_score
      response['gw_score'] = hasildebat.gw_score
      response['oo'] = hasildebat.oo
      response['dlo'] = hasildebat.dlo
      response['ow'] = hasildebat.ow
      response['oo_score'] = hasildebat.oo_score
      response['dlo_score'] = hasildebat.dlo_score
      response['ow_score'] = hasildebat.ow_score
      response['general_comment'] = hasildebat.general_comment  
      return render(request, 'mengumumkanhasil/ubah_hasil.html', response)


def hapus_hasil(request, id_hasil):
    user = request.user
    juri = Juri.objects.filter(user=user)
    
    if (len(juri) < 1):
      return redirect('mengumumkanhasil:daftar_hasil')
    
    juri = juri[0]
    juri.hapusHasilDebat(id_hasil)
    return redirect('mengumumkanhasil:dasbor_hasil')

# Other Function

def isValid(hasil):
    pm = hasil.pm
    dpm = hasil.dpm
    gw = hasil.gw
    pm_score  = hasil.pm_score
    dpm_score = hasil.dpm_score
    gw_score = hasil.gw_score
    oo = hasil.oo
    dlo = hasil.dlo
    ow = hasil.ow
    oo_score = hasil.oo_score
    dlo_score = hasil.dlo_score
    ow_score = hasil.ow_score
    general_comment = hasil.general_comment
    if pm == "" or pm == None or \
        dpm == "" or dpm == None or \
         gw == "" or gw == None or \
            pm_score == "" or pm_score  == None or \
              dpm_score == "" or dpm_score == None or \
                gw_score == "" or gw_score == None or \
                 oo== "" or oo == None or \
                    dlo == "" or dlo == None or \
                      ow == "" or ow == None or \
                        oo_score== "" or oo_score == None or \
                          dlo_score == "" or dlo_score == None or \
                            ow_score == "" or ow_score == None or \
                              general_comment == "" or general_comment == None :
      return False
    return True