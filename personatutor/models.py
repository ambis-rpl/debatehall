from typing import DefaultDict
from django.db import models

# Create your models here.

class PersonaTutor(models.Model):
    waktu = (
        ('H', '1 Hari'),
        ('M', '1 Minggu'),
        ('B', '1 Bulan'),
    )
    nama = models.CharField(max_length=50, blank=True)
    email = models.EmailField(blank=True)
    tentang_tutor = models.TextField(max_length=1000, blank=True)
    deskripsi_kursus = models.TextField(max_length=1000, blank=True)
    jadwal = models.CharField(max_length=1, choices=waktu, blank=True)
    harga = models.PositiveIntegerField(blank=True, null=True)
    ready = models.BooleanField(default=False)

    def save(self, *args, **kwargs):
        self.ready = True

        if not self.nama:
            self.ready = False
        if not self.email:
            self.ready = False
        if not self.tentang_tutor:
            self.ready = False
        if not self.deskripsi_kursus:
            self.ready = False
        if not self.jadwal:
            self.ready = False
        if not self.harga:
            self.ready = False

        super(PersonaTutor, self).save(*args, **kwargs)
    
    def __str__(self):
        if self.nama:
            return self.nama
        else:
            return "Unknown"
