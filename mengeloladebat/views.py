
from django.http import HttpResponse
from django.shortcuts import render, redirect
from mengeloladebat.models import Debat
from main.models import HasilDebat, Juri
from .forms import debatForm

def adddebat(request):
    form_tambah = debatForm()
    response = {
        'form_tambah': form_tambah,
    }
    return render(request, 'addDebat.html', response)

def post_debat(request):
    if request.method == 'POST':
        form = debatForm(request.POST or None, request.FILES or None)
        response_data = {}
        if form.is_valid():
            response_data['iddebat'] = request.POST['iddebat']
            response_data['judul'] = request.POST['judul']
            response_data['deskripsi'] = request.POST['deskripsi']
            response_data['starttime'] = request.POST['starttime']
            response_data['endtime'] = request.POST['endtime']
            response_data['link'] = request.POST['link']
            response_data['status'] = request.POST['status']

            data_debat = Debat(iddebat=response_data['iddebat'],
                                 judul=response_data['judul'],
                                 deskripsi=response_data['deskripsi'],
                                 starttime=response_data['starttime'],
                                 endtime=response_data['endtime'],
                                 link=response_data['link'],
                                 status=response_data['status'],
                                 )
            data_debat.save()
            return redirect('/mengeloladebat/listdebat')

        else:
            return redirect('/mengeloladebat/adddebat')
    else:
        return redirect('/mengeloladebat/adddebat')


def list_debat(request):
    form_tambah = debatForm()
    data = Debat.objects.all()
    response = {
        'form_tambah': form_tambah,
        'data': data,
    }
    return render(request, 'listDebat.html', response)


def detail_debat(request, id_debat):
    data = Debat.objects.filter(iddebat=id_debat)
    hasil = HasilDebat.objects.filter(id_hasil=id_debat)
    if (len(hasil) < 1):
        hasilIsExist = False
    else:
        hasilIsExist = True
    
    response = {
        'data': data,
        'hasil': hasil,
    }
    response['hasilIsExist'] = hasilIsExist
    user = request.user
    response['isJuri'] = False
    if (user.is_authenticated):
      juri = Juri.objects.filter(user=user)
      if (len(juri) > 0):
        response['isJuri'] = True
    return render(request, 'detailDebat.html', response)

def update(request,id_debat):
    if request.method == 'POST':
        a = Debat.objects.get(iddebat=id_debat)
        form = debatForm(request.POST or None, request.FILES or None, instance = a)
        response_data = {}
        if form.is_valid():
            form.save()
            return redirect('/mengeloladebat/listdebat')

        else:
            return redirect('/mengeloladebat/adddebat')
    else:
        return redirect('/mengeloladebat/adddebat')

