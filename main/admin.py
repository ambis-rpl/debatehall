from django.contrib import admin
from .models import Artikel, Juri, Pendebat, Host, Reservasi, Tutor

# Register your models here.
admin.site.register(Juri)
admin.site.register(Pendebat)
admin.site.register(Host)
admin.site.register(Tutor)
admin.site.register(Artikel)
admin.site.register(Reservasi)
