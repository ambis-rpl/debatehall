from django.contrib import admin
from .models import PersonaTutor

# Register your models here.
admin.site.register(PersonaTutor)