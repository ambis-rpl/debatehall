from django import forms


class debatForm(forms.Form):
    error_messages = {
        'required': 'This field is required.'
    }
    attrs = {
        'class': 'form-control'
    }
    
    iddebat = forms.CharField(widget=forms.TextInput(attrs=attrs), label='ID Debat', max_length=50, required=True)
    judul = forms.CharField(widget=forms.TextInput(attrs=attrs), label='Judul Debat', max_length=50, required=True)
    deskripsi = forms.CharField(widget=forms.TextInput(attrs=attrs), label='Deskripsi Singkat ',
                                required=True)
    starttime = forms.CharField(widget=forms.TextInput(attrs=attrs), label='Start Time', max_length=50, required=True)
    endtime = forms.CharField(widget=forms.TextInput(attrs=attrs), label='End Time', max_length=50,
                                required=True)
    link = forms.CharField(widget=forms.TextInput(attrs=attrs), label='Link Debat', max_length=50,
                              required=True)
    status = forms.CharField(widget=forms.TextInput(attrs=attrs), label='Status Debat', max_length=50,
                              required=True)
 