from django.urls import path

from . import views

app_name = 'reservasi'

urlpatterns = [
    path('', views.list_reservasi, name='list-reservasi'),
    path('<int:id>/create', views.create_reservasi, name='create'),
    path('<int:id>/bayar', views.bayar_reservasi, name='bayar'),
    path('<int:id>/cancel', views.cancel_reservasi, name='cancel'),
]
