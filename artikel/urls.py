from django.urls import path

from artikel import views

app_name = 'artikel'

urlpatterns = [
    path('', views.daftar_artikel, name='daftar_artikel'),
    path('<int:id_artikel>/', views.detail_artikel, name='detail_artikel'),
    path('dasbor/', views.getAllArtikel, name='dasbor_artikel'),
    path('buat/', views.showCreateArtikelForm, name='buat_artikel'),
    path('ubah/<int:id_artikel>/', views.showFormWithDetailArtikel, name='ubah_artikel'),
    path('hapus/<int:id_artikel>/', views.hapus_artikel, name='hapus_artikel'),
]
