from django.apps import AppConfig


class MengeloladebatConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'mengeloladebat'
