# Generated by Django 3.2.9 on 2021-12-08 08:05

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0002_alter_reservasi_created_at'),
    ]

    operations = [
        migrations.AlterField(
            model_name='reservasi',
            name='created_at',
            field=models.DateTimeField(default=datetime.datetime(2021, 12, 8, 8, 5, 16, 179474, tzinfo=utc), editable=False),
        ),
    ]
