from django.urls import path
from . import views

urlpatterns = [
    path('adddebat', views.adddebat),
    path('post', views.post_debat),
    path('listdebat', views.list_debat),
    path('updatedebat/<id>', views.update),
    path('<str:id_debat>', views.detail_debat),
    
]
