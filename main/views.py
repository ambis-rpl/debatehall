from django.shortcuts import redirect, render
from django.contrib.auth import authenticate, login, logout
from .models import Pendebat, Tutor, Juri, Host
from django.contrib.auth.models import User


def home(request):
    return render(request, 'main/home.html')

def loginView(request):
    if request.method == 'POST':
      name = request.POST.get('name')
      password = request.POST.get('password')
      user = authenticate(username=name, password=password)
      if user is not None:
        if len(Pendebat.objects.filter(user=user)) > 0:
          request.session['role'] = 'pendebat'
        elif len(Juri.objects.filter(user=user)) > 0:
          request.session['role'] = 'juri'
        elif len(Tutor.objects.filter(user=user)) > 0:
          request.session['role'] = 'tutor'
        elif len(Host.objects.filter(user=user)) > 0:
          request.session['role'] = 'host'
        else:
          return render(request, 'main/login.html')
        login(request, user)
        return redirect('main:home')

    return render(request, 'main/login.html')

def register(request):
    if request.method == 'POST':
      email = request.POST.get('email')
      name = request.POST.get('name')
      role = request.POST.get('role')
      password = request.POST.get('password')
      User.objects.create_user(username=name, password=password, email=email)
      user = authenticate(username=name, password=password)
      if (role == 'pendebat'):
        pengguna = Pendebat.objects.create(user = user)

      elif (role == 'tutor'):
        pengguna = Tutor.objects.create(user = user)

      elif (role == 'juri'):
        pengguna = Juri.objects.create(user = user)
        
      elif (role == 'host'):
        pengguna = Host.objects.create(user = user)
      else:
        return render(request, 'main/register.html')
      
      request.session["role"] = role
      pengguna.save()
      login(request, user)
      return redirect("main:home")

    else:
      return render(request, 'main/register.html')

def logoutView(request):
    request.session.flush()
    logout(request)
    return redirect('main:home')

def dummy_login(request):
    return render(request, 'main/home_loggedin.html')
