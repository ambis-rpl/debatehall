from django.db import models
from mengeloladebat.models import Debat
from main.models import Juri
from tinymce import HTMLField
from datetime import datetime

# Create your models here.

# class HasilDebat(models.Model):
#     id_hasil = models.AutoField(primary_key=True)
#     debatModel = models.ForeignKey(Debat, blank=True, null=True, on_delete=models.SET_NULL)
#     pm = models.CharField(max_length=50)
#     dpm = models.CharField(max_length=50)
#     gw = models.CharField(max_length=50)
#     pm_score = models.IntegerField(null=True, blank=True)
#     dpm_score = models.IntegerField(null=True, blank=True)
#     gw_score = models.IntegerField(null=True, blank=True)
#     oo = models.CharField(max_length=50)
#     dlo = models.CharField(max_length=50)
#     ow = models.CharField(max_length=50)
#     oo_score = models.IntegerField(null=True, blank=True)
#     dlo_score = models.IntegerField(null=True, blank=True)
#     ow_score = models.IntegerField(null=True, blank=True)
#     general_comment = HTMLField('Content')
#     juri_pembuat = models.ForeignKey(Juri, blank=True, null=True, on_delete=models.SET_NULL)
 
#     def __str__(self):
#         return "hasil debat " + self.debatModel.judul