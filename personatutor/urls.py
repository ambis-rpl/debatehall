from django.urls import path

from personatutor.views import show_create_persona_form, show_delete_persona, show_edit_persona_form, show_persona_tutor, show_persona_tutor_list 

app_name = 'personatutor'

urlpatterns = [
    path('', show_persona_tutor_list, name='daftar'),
    path('createpersonatutor', show_create_persona_form, name='create'),
    path('<int:id>', show_persona_tutor, name='show'),
    path('<int:id>/edit', show_edit_persona_form, name='edit'),
    path('<int:id>/delete', show_delete_persona, name='delete'),
]