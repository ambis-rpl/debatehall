from django.http import response
from django.shortcuts import redirect, render
# from .models import Artikel
from main.models import Host, Artikel

# Controller

def daftar_artikel(request):
    response = {}
    user = request.user
    response['isHost'] = False
    if (user.is_authenticated):
      host = Host.objects.filter(user=user)
      if (len(host) > 0):
        response['isHost'] = True

    response['artikels'] = Artikel.objects.all()
    return render(request, 'daftar_artikel.html', response)

def detail_artikel(request, id_artikel):
    response = {}
    response['artikel'] = Artikel.objects.filter(id_artikel=id_artikel)[0]
    return render(request, 'detail_artikel.html', response)

def getAllArtikel(request):
    response = {}
    user = request.user
    host = Host.objects.filter(user=user)
    if (len(host) < 1):
      return redirect('artikel:daftar_artikel')
    
    host = host[0]
    response['artikels'] = Artikel.objects.all()
    return render(request, 'dasbor_artikel.html', response)

def showCreateArtikelForm(request):
    response = {}
    user = request.user
    host = Host.objects.filter(user=user)
    if (len(host) < 1):
      return redirect('artikel:daftar_artikel')
    
    host = host[0]
    if request.method == 'POST':
      judul = request.POST.get('judul')
      deskripsi = request.POST.get('deskripsi')
      artikel = Artikel(judul=judul, deskripsi=deskripsi, host_pembuat=host)
      if isValid(artikel):
        host.buatArtikel(artikel)
        return redirect('artikel:dasbor_artikel')
      else:
        response['err_msg'] = True
        response['judul'] = judul
        response['deskripsi'] = deskripsi
        return render(request, 'buat_artikel.html', response)

    else:
      return render(request, 'buat_artikel.html', response)

def showFormWithDetailArtikel(request, id_artikel):
    response = {}
    user = request.user
    host = Host.objects.filter(user=user)
    if (len(host) < 1):
      return redirect('artikel:daftar_artikel')
    
    host = host[0]
    response['id_artikel'] = id_artikel
    if request.method == 'POST':
      judul = request.POST.get('judul')
      deskripsi = request.POST.get('deskripsi')
      artikel = Artikel(judul=judul, deskripsi=deskripsi)
      if isValid(artikel):
        host.ubahArtikel(id_artikel, artikel)
        return redirect('artikel:dasbor_artikel')
      else:
        response['err_msg'] = True
        response['judul'] = judul
        response['deskripsi'] = deskripsi
        return render(request, 'ubah_artikel.html', response)        

    else:
      artikel = Artikel.objects.get(id_artikel=id_artikel)
      response['judul'] = artikel.judul
      response['deskripsi'] = artikel.deskripsi
      return render(request, 'ubah_artikel.html', response)


def hapus_artikel(request, id_artikel):
    user = request.user
    host = Host.objects.filter(user=user)
    if (len(host) < 1):
      return redirect('artikel:daftar_artikel')
    
    host = host[0]
    host.hapusArtikel(id_artikel)
    return redirect('artikel:dasbor_artikel')

# Other Function

def isValid(artikel):
    judul = artikel.judul
    deskripsi = artikel.deskripsi
    if judul == "" or judul == None or deskripsi == "" or deskripsi == None:
      return False
    return True
