from django.db import models
from django.contrib.auth.models import User
from personatutor.models import PersonaTutor
from tinymce import HTMLField
from datetime import datetime
from mengeloladebat.models import Debat
# from mengumumkanhasil.models import HasilDebat
from django.utils import timezone

class Pengguna(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    bio = models.TextField(max_length=500, blank=True)

class PesertaKD(Pengguna):
    jumlah_kegiatan_debat = models.IntegerField(default=0)

class Juri(PesertaKD):
    link_cv_resume = models.CharField(max_length=100)
    def buatHasilDebat(self, hasildebat):
        hasildebat.save()
        
    def ubahHasilDebat(self, id_hasildebat, hasildebat):
        oldHasilDebat = HasilDebat.objects.get(id_hasil=id_hasildebat)
        oldHasilDebat.pm = hasildebat.pm
        oldHasilDebat.dpm = hasildebat.dpm
        oldHasilDebat.gw = hasildebat.gw
        oldHasilDebat.pm_score = hasildebat.pm_score
        oldHasilDebat.dpm_score = hasildebat.dpm_score
        oldHasilDebat.gw_score = hasildebat.gw_score
        
        oldHasilDebat.oo = hasildebat.oo
        oldHasilDebat.dlo = hasildebat.dlo
        oldHasilDebat.ow = hasildebat.ow
        oldHasilDebat.oo_score = hasildebat.oo_score
        oldHasilDebat.dlo_score = hasildebat.dlo_score
        oldHasilDebat.ow_score = hasildebat.ow_score

        oldHasilDebat.general_comment = hasildebat.general_comment
        oldHasilDebat.save()

    def hapusHasilDebat(self, id_hasil):
        hasilDebat = HasilDebat.objects.get(id_hasil=id_hasil)
        hasilDebat.delete()

    def __str__(self):
      return self.user.username

class Pendebat(PesertaKD):
    def createReservasi(self, id_tutor):
        pendebat = Pendebat.objects.get(id=self.id)
        tutor = Tutor.objects.get(id=id_tutor)
        res = Reservasi(pendebat=pendebat, tutor=tutor)
        res.save()
        return True

    def bayarReservasi(self, id_res):    
        pendebat = Pendebat.objects.get(id=self.id)
        res = Reservasi.objects.get(id_reservasi=id_res, pendebat=pendebat)
        if res:
            res.status_reservasi = "Completed"
            res.modified_at = timezone.now()
            res.save()
            return True

    def cancelReservasi(self, id_res):
        pendebat = Pendebat.objects.get(id=self.id)
        res = Reservasi.objects.get(id_reservasi=id_res, pendebat=pendebat)
        if res:
            res.status_reservasi = "Cancelled"
            res.modified_at = timezone.now()
            res.save()
            return True

class Host(Pengguna):
    jumlah_kegiatan_debat = models.IntegerField(default=0)

    def buatArtikel(self, artikel):
        artikel.save()

    def ubahArtikel(self, id_artikel, artikel):
        oldArtikel = Artikel.objects.get(id_artikel=id_artikel)
        oldArtikel.judul = artikel.judul
        oldArtikel.deskripsi = artikel.deskripsi
        oldArtikel.save()

    def hapusArtikel(self, id_artikel):
        artikel = Artikel.objects.get(id_artikel=id_artikel)
        artikel.delete()

    def __str__(self):
      return self.user.username


class Tutor(Pengguna):
    link_cv_resume = models.CharField(max_length=100)
    jumlah_reservant = models.IntegerField(default=0)
    persona = models.ForeignKey(PersonaTutor, on_delete=models.SET_NULL, null=True)

    def buatPersona(self, persona):
        obj = persona.save()
        self.persona = obj
        self.save()

    def ubahPersona(self, persona):
        persona.save()

    def hapusPersona(self, id):
        persona = PersonaTutor.objects.get(id=id)
        persona.delete()

    def __str__(self):
      return self.user.username

class Artikel(models.Model):
    id_artikel = models.AutoField(primary_key=True)
    judul = models.CharField(max_length=100)
    waktu_dibuat = models.DateField(default=datetime.now)
    deskripsi = HTMLField('Content')
    host_pembuat = models.ForeignKey(Host, blank=True, null=True, on_delete=models.SET_NULL)

    def __str__(self):
        return self.judul
    
class HasilDebat(models.Model):
    id_hasil = models.AutoField(primary_key=True)
    debatModel = models.ForeignKey(Debat, blank=True, null=True, on_delete=models.SET_NULL)
    pm = models.CharField(max_length=50)
    dpm = models.CharField(max_length=50)
    gw = models.CharField(max_length=50)
    pm_score = models.IntegerField(null=True, blank=True)
    dpm_score = models.IntegerField(null=True, blank=True)
    gw_score = models.IntegerField(null=True, blank=True)
    oo = models.CharField(max_length=50)
    dlo = models.CharField(max_length=50)
    ow = models.CharField(max_length=50)
    oo_score = models.IntegerField(null=True, blank=True)
    dlo_score = models.IntegerField(null=True, blank=True)
    ow_score = models.IntegerField(null=True, blank=True)
    general_comment = HTMLField('Content')
    juri_pembuat = models.ForeignKey(Juri, blank=True, null=True, on_delete=models.SET_NULL)
 
    def __str__(self):
        return "hasil debat " + self.debatModel.judul

class Reservasi(models.Model):
    id_reservasi = models.AutoField(primary_key=True)
    created_at = models.DateTimeField(default=timezone.now(), editable=False)
    modified_at = models.DateTimeField(blank=True, null=True)
    status_reservasi = models.CharField(max_length=20, default="Menunggu Pembayaran")
    pendebat = models.ForeignKey(Pendebat, on_delete=models.SET_NULL, null=True)
    tutor = models.ForeignKey(Tutor, on_delete=models.SET_NULL, null=True)
