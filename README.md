# debateHall

[![pipeline status][pipeline-badge]][commits-gl]
[![coverage report][coverage-badge]][commits-gl]

## Website

https://debatehall.herokuapp.com/

## Anggota
- Amanda Carrisa Ashardian – 1906399966
- Ferdi Fadillah – 1906351083
- Muhammad Fayaad – 1906398433
- Natasya Zahra – 1906293253
- Naufal Pramudya Yusuf – 1906293266

## Fitur 
- Sarana Lomba Debat Online
- Tutor Pengajar
- Artikel

[pipeline-badge]: https://gitlab.com/ambis-rpl/debatehall/badges/develop/pipeline.svg
[coverage-badge]: https://gitlab.com/ambis-rpl/debatehall/badges/develop/coverage.svg
[commits-gl]: https://gitlab.com/ambis-rpl/debatehall/-/commits/develop
