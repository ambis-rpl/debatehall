from django import forms
from .models import PersonaTutor

class Form(forms.ModelForm):
    class Meta:
        model = PersonaTutor
        fields = ['nama', 'email', 'tentang_tutor', 'deskripsi_kursus', 'jadwal', 'harga']
 