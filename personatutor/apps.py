from django.apps import AppConfig


class PersonatutorConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'personatutor'
