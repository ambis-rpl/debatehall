from django.contrib.auth.models import User
from django.shortcuts import redirect, render, HttpResponseRedirect
from django.contrib import messages
from main.models import Pendebat, Tutor, Reservasi
# Create your views here.

def list_reservasi(request):
    user = request.user
    pendebat = Pendebat.objects.filter(user=user)[0]
    data_reservasi = Reservasi.objects.filter(pendebat = pendebat)
    context = {'data_reservasi': data_reservasi}
    return render(request, 'reservasi/list-reservasi.html', context)

def create_reservasi(request, id):
    user = request.user
    pendebat = Pendebat.objects.filter(user=user)[0]
    reservasi = pendebat.createReservasi(id)
    if reservasi:
        return redirect('reservasi:list-reservasi')

    messages.error(request, "Reservasi gagal dibuat")
    return HttpResponseRedirect('tutor/')

def bayar_reservasi(request, id):
    user = request.user
    pendebat = Pendebat.objects.filter(user=user)[0]
    pay = pendebat.bayarReservasi(id)
    if pay:
        messages.success(request, "Reservasi berhasil dibayar!")
        return HttpResponseRedirect('/reservasi-tutor/')
    else:
        messages.error(request, "Pembayaran gagal diproses")
        return HttpResponseRedirect('/reservasi-tutor/')

def cancel_reservasi(request, id):
    user = request.user
    pendebat = Pendebat.objects.filter(user=user)[0]
    cancel = pendebat.cancelReservasi(id)
    if cancel:
        print(Reservasi.objects.all())
        messages.success(request, "Reservasi berhasil dibatalkan!")
        return HttpResponseRedirect('/reservasi-tutor/')
    else:
        print(Reservasi.objects.all())
        messages.error(request, "Pembatalan gagal diproses")
        return HttpResponseRedirect('/reservasi-tutor/')