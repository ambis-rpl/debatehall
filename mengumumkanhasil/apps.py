from django.apps import AppConfig


class MengumumkanhasilConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'mengumumkanhasil'
