from django.urls import path

from . import views

app_name = 'mengumumkanhasil'

urlpatterns = [
    #path('/formulir_hasil', views.formulir_hasil, name='formulir_hasil'),
    path('', views.daftar_hasil, name='daftar_hasil'),
    path('hasil_debat', views.hasil_debat, name='hasil_debat'),
    path('dasbor/', views.getAllHasil, name='dasbor_hasil'),
    path('<int:id_hasil>/', views.detail_hasil, name='detail_hasil'),
    path('ubah/<int:id_hasil>/', views.showFormWithDetailArtikel, name='ubah_hasil'),
    path('formulir_hasil', views.showCreateArtikelForm, name='formulir_hasil'),
    path('hapus/<int:id_hasil>/', views.hapus_hasil, name='hapus_hasil'),
]
