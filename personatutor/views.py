from django.shortcuts import (get_object_or_404, render, redirect, HttpResponseRedirect)
from django.urls import reverse
from main.models import Pendebat, Tutor
from .forms import Form
from .models import PersonaTutor

# Create your views here.
def show_persona_tutor(request, id):
    persona = PersonaTutor.objects.get(id=id)
    tutor = Tutor.objects.get(persona=persona)
    show_option = False
    show_reservasi = False
    try:
        if Tutor.objects.get(id=request.user.id).persona == persona:
            show_option = True
        if Pendebat.objects.get(id=request.user.id):
            show_reservasi = True
    except:
        pass

    response = {
        'data' : persona,
        'tutor' : tutor,
        'show_option': show_option,
        'show_reservasi': show_reservasi
    }
    return render(request, 'personatutor/personatutor.html', response)

def show_persona_tutor_list(request):
    show_id = 0
    create = False
    try:
        tutor = Tutor.objects.get(id=request.user.id)
        print(tutor)
        print(tutor.persona)
        if tutor:
            if not tutor.persona:
                create = True
            else:
                show_id = tutor.persona.id
    except:
        pass

    response = {
        'dataset' : PersonaTutor.objects.filter(ready=True),
        'create' : create,
        'show_id' : show_id,
    }
    return render(request, 'personatutor/personatutorlist.html', response)

def show_create_persona_form(request):
    form = Form(request.POST or None)
    tutor = request.user
    is_tutor = False

    try:
        tutor = Tutor.objects.get(id=request.user.id)
        is_tutor = True
    except:
        pass

    if form.is_valid() and request.method =='POST':
        tutor.buatPersona(form)
        return HttpResponseRedirect(reverse('personatutor:daftar'))

    response = {
        'form' : form,
        'tutor': tutor,
        'is_tutor' : is_tutor
    }

    return render(request, 'personatutor/personatutorcreate.html', response)

def show_edit_persona_form(request, id):
    obj = get_object_or_404(PersonaTutor, id=id)
    form = Form(request.POST or None, instance=obj)
    tutor = request.user
    try:
        tutor = Tutor.objects.get(id=request.user.id)
    except:
        pass

    if form.is_valid() and request.method =='POST':
        tutor.ubahPersona(form)
        return HttpResponseRedirect(reverse('personatutor:show', args=[id]))

    response = {
        'form' : form,
        'id' : id,
        'tutor' : tutor,
    }
    return render(request, 'personatutor/personatutoredit.html', response)

def show_delete_persona(request, id):
    try:
        tutor = Tutor.objects.get(id=request.user.id)

        if tutor and int(tutor.persona.id) == int(id):
            tutor.hapusPersona(id)
            return redirect('personatutor:daftar')
    except:
        return render(request, 'personatutor/personatutorerror.html')