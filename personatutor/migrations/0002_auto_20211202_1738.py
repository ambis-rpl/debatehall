# Generated by Django 3.2.9 on 2021-12-02 10:38

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('personatutor', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='personatutor',
            name='ready',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='personatutor',
            name='deskripsi_kursus',
            field=models.TextField(blank=True, max_length=1000),
        ),
        migrations.AlterField(
            model_name='personatutor',
            name='email',
            field=models.EmailField(blank=True, max_length=254),
        ),
        migrations.AlterField(
            model_name='personatutor',
            name='harga',
            field=models.PositiveIntegerField(blank=True),
        ),
        migrations.AlterField(
            model_name='personatutor',
            name='jadwal',
            field=models.CharField(blank=True, choices=[('H', '1 Hari'), ('M', '1 Minggu'), ('B', '1 Bulan')], max_length=1),
        ),
        migrations.AlterField(
            model_name='personatutor',
            name='nama',
            field=models.CharField(blank=True, max_length=50),
        ),
        migrations.AlterField(
            model_name='personatutor',
            name='tentang_tutor',
            field=models.TextField(blank=True, max_length=1000),
        ),
    ]
