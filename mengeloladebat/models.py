from django.db import models


# Create your models here.

class Debat(models.Model):
    iddebat = models.CharField(max_length=100)
    judul = models.CharField(max_length=100)
    deskripsi = models.CharField(max_length=100)
    starttime = models.CharField(max_length=100)
    endtime = models.CharField(max_length=100)
    link = models.CharField(max_length=100)
    status = models.CharField(max_length=100)
 
    def __str__(self):
        return self.judul
