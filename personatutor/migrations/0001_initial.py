# Generated by Django 3.2.9 on 2021-12-01 02:08

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='PersonaTutor',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nama', models.CharField(max_length=50)),
                ('email', models.EmailField(max_length=254)),
                ('tentang_tutor', models.TextField(max_length=1000)),
                ('deskripsi_kursus', models.TextField(max_length=1000)),
                ('jadwal', models.CharField(choices=[('H', '1 Hari'), ('M', '1 Minggu'), ('B', '1 Bulan')], max_length=1)),
                ('harga', models.PositiveIntegerField()),
            ],
        ),
    ]
